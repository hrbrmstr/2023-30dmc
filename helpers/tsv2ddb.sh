#!/bin/bash
# TSV to DuckDB
# TODO make `sep` an input parameter
# usage: ./tsv2duckdb.sh bigdata.tsv duckdbfilename tablename

set -e

TSV_FILE="${1}"
DUCKDB_FILENAME="${2}"
TABLE_NAME="${3}"

TMPDIR=$(mktemp -d)
TMPTSV="${TMPDIR}/TSV.tsv"

head -n 1001 "${TSV_FILE}" > "${TMPTSV}"

duckdb "${DUCKDB_FILENAME}" <<EOF
.mode csv
.sep '\t'
CREATE TABLE ${TABLE_NAME} AS 
  SELECT * FROM read_csv_auto('${TMPTSV}', HEADER=TRUE);
EOF
rm "${TMPTSV}"

duckdb "${DUCKDB_FILENAME}" <<EOF
.mode csv
.sep '\t'
DELETE FROM ${TABLE_NAME}; 
INSERT INTO ${TABLE_NAME} 
  SELECT * FROM read_csv_auto('${TSV_FILE}', HEADER=TRUE);
EOF

duckdb "${DUCKDB_FILENAME}" <<EOF
SELECT 
  COUNT(*) aS row_count
FROM ${TABLE_NAME};
.quit
EOF
