render:
  quarto render

sync:
  rsync -avp _book/* bob@hetzner-01:/usr/share/30dmc/2023/

git:
  git add -A
  git commit -m "chore: update"
  git push